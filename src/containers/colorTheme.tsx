import { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createContainer } from 'unstated-next';
import { DarkTheme, EditorTheme, LightTheme } from '@/src/theme';

export const useColorThemeProvider = createContainer((theme: EditorTheme = LightTheme) => {
    const [colorMode, _setColorMode] = useState<'light' | 'dark'>('light')
    const [colors, setColors] = useState(theme)

    useEffect(() => {
        const getDefault = async () => {
            const res = await AsyncStorage.getItem('colorMode')
            if (res && (res === 'dark' || res === 'light')) {
                _setColorMode(res)
                setColors(colorMode === 'dark' ? DarkTheme : LightTheme)
            }
        };
        getDefault();
    }, [])

    const setColorMode = (value: 'light' | 'dark') => {
        AsyncStorage.setItem('colorMode', value);
        _setColorMode(value);
        if (value === 'dark') {
            setColors(DarkTheme)
        } else {
            setColors(LightTheme)
        }
    };

    return {
        setColorMode,
        colorMode,
        colors
    };
});

export const ColorThemeProvider = useColorThemeProvider.Provider;
export const useColorState = useColorThemeProvider.useContainer;