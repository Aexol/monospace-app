import {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createContainer} from 'unstated-next';

const useTokenStateContainer = createContainer(() => {
  const [token, _setToken] = useState<string | undefined>();
  const [userName, _setUserName] = useState<string | undefined>();
  const [phone, _setPhone] = useState<string | undefined>();
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false)
  const [loadingToken, setLoadingToken] = useState<boolean>(false)


  useEffect(() => {
    const getFromAsync = () => {
      setLoadingToken(true)
      AsyncStorage.getItem('token').then(t => {
        if (t) {
          _setToken(t);
          setIsAuthenticated(true);
        } else {
          setIsAuthenticated(false);
        }
      });
      AsyncStorage.getItem('userName').then(t => {
        if (t) {
          _setUserName(t);
        } 
      });
      AsyncStorage.getItem('phone').then(t => {
        if (t) {
          _setPhone(t);
        }
      });
      setLoadingToken(false)
    }
    getFromAsync()
  }, [])

  const setToken = (value: string) => {
    AsyncStorage.setItem('token', value);
    _setToken(value);
  };
  const setUserName = (value: string) => {
    AsyncStorage.setItem('userName', value);
    _setUserName(value);
  };
  const setPhone = (value: string) => {
    AsyncStorage.setItem('phone', value);
    _setPhone(value);
  };

  const clearAllTokens = () => {
    AsyncStorage.removeItem('token');
    setToken('');
    setIsAuthenticated(false);
  }

  const logout = () => {
    AsyncStorage.removeItem('token');
    AsyncStorage.removeItem('userName');
    setIsAuthenticated(false);
};

  return {
    clearAllTokens,
    isAuthenticated,
    setIsAuthenticated,
    loadingToken,
    token,
    phone,
    setToken,
    userName,
    setUserName,
    setPhone,
    logout
  };
});

export const TokenStateProvider = useTokenStateContainer.Provider;
export const useTokenState = useTokenStateContainer.useContainer;
