import { useBackend } from '@/src/hooks/backend';
import { ZeusHook } from '@/src/zeus';
import { useState } from 'react';
import { DocumentPickerResponse } from 'react-native-document-picker';
import { createContainer } from 'unstated-next';

const useContentStateContainer  = createContainer(() => {
    const [content, setContent] = useState<string>('');
    const [filterMode, setFilterMode] = useState<boolean>(false)
    const [userArticles, setUserArticles] = useState<ZeusHook<typeof useBackend, 'getWall'>>([])
    const [filteredContent, setFilteredContent] = useState<ZeusHook<typeof useBackend, 'getWall'>>([])
    const [fileSelected, setFileSelected] = useState<DocumentPickerResponse>();

    return {
        content, setContent, userArticles, setUserArticles, fileSelected, setFileSelected, filteredContent, setFilteredContent, filterMode, setFilterMode
    };
});

export const ContentStateProvider = useContentStateContainer.Provider;
export const useContentState = useContentStateContainer.useContainer;
