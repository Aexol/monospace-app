import * as React from "react"
import Svg, { SvgProps, Path } from "react-native-svg"

export const SendLink = (props: SvgProps) => {
    return (
        <Svg
            width={18}
            height={20}
            fill="none"
            {...props}
        >
            <Path
                d="M15 14.13c-.76 0-1.445.296-1.964.771L5.91 10.746c.055-.226.09-.461.09-.701 0-.24-.035-.474-.09-.7l7.05-4.115c.552.52 1.282.81 2.04.81 1.655 0 3-1.345 3-3s-1.345-3-3-3-3 1.345-3 3c0 .24.035.475.09.7L5.04 7.855A2.982 2.982 0 003 7.045c-1.655 0-3 1.345-3 3s1.345 3 3 3c.758 0 1.488-.29 2.04-.81l7.126 4.155c-.053.215-.08.435-.08.655A2.913 2.913 0 1015 14.132z"
                fill="#C4C4C4"
            />
        </Svg>
    )
}

export default SendLink