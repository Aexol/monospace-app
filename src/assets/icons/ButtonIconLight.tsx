import * as React from "react"
import Svg, { SvgProps, Rect, Path } from "react-native-svg"

export const ButtonIconLight = (props: SvgProps): JSX.Element => {
    return (
        <Svg
            width={32}
            height={32}
            fill="none"
            {...props}
        >
            <Rect width={32} height={32} rx={8} fill="#414141" />
            <Path
                d="M8.42 24.579a.998.998 0 01-.737-.326.988.988 0 01-.263-.764l.245-2.694L18.983 9.48l3.537 3.536L11.205 24.33l-2.694.245a.953.953 0 01-.091.004zm14.806-12.27L19.69 8.775l2.121-2.121a1.001 1.001 0 011.415 0l2.121 2.12a1 1 0 010 1.416l-2.12 2.12h-.001z"
                fill="#fff"
            />
        </Svg>
    )
}
