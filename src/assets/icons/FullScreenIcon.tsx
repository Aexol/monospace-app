import * as React from "react"
import Svg, { SvgProps, Path } from "react-native-svg"

export const FullScreenIcon = (props: SvgProps) => {
    return (
        <Svg
            width="1em"
            height="1em"
            viewBox="0 0 17 16"
            fill="none"
            {...props}
        >
            <Path
                d="M8.744 1.778a.444.444 0 000 .889h4.263l-4.112 4.11a.445.445 0 10.627.627l4.111-4.11v4.262a.445.445 0 00.889 0V1.778H8.744zM2.077 8.444a.444.444 0 11.889 0v4.263l4.111-4.111a.444.444 0 11.627.626l-4.111 4.111h4.262a.445.445 0 010 .89H2.077V8.443z"
                fill="#fff"
            />
        </Svg>
    )
}