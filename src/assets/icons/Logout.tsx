import * as React from "react"
import Svg, { SvgProps, Path } from "react-native-svg"

export const Logout = (props: SvgProps): JSX.Element => {
    return (
        <Svg
            width={14}
            height={14}
            {...props}
        >
            <Path
                d="M9 13h2.667A1.334 1.334 0 0013 11.667V2.333A1.334 1.334 0 0011.667 1H9M4.333 10.333L1 7l3.333-3.333M1 7h8"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    )
}
