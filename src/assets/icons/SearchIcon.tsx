import * as React from "react"
import Svg, { SvgProps, Path } from "react-native-svg"

export const SearchIcon = (props: SvgProps) => {
    return (
        <Svg
            width={19}
            height={19}
            fill="none"
            {...props}
        >
            <Path
                d="M17.71 16.29l-3.4-3.39A7.92 7.92 0 0016 8a8 8 0 10-8 8 7.92 7.92 0 004.9-1.69l3.39 3.4a1.002 1.002 0 001.639-.325 1 1 0 00-.219-1.095zM2 8a6 6 0 1112 0A6 6 0 012 8z"
                fill="#F2F2F2"
            />
        </Svg>
    )
}

export default SearchIcon
