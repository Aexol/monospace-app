import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';
const { width } = Dimensions.get('window');

interface props {
    videoId: string
}

export const CustomYouTubePlayer: React.FC<props> = ({ videoId }) => {
    return (
        <YoutubePlayer
            height={width * 0.55}
            width={width * 0.94}
            play={false}
            volume={800}
            videoId={videoId}
            initialPlayerParams={{ preventFullScreen: true }}
            webViewStyle={styles.webViewStyle}
        />
    )
}

const styles = StyleSheet.create({
    webViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 0,
        marginBottom: 0,
        width: '100%',
    },
})