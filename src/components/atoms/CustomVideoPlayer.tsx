import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components/native';
import { Dimensions, StatusBar, View } from 'react-native';
import Orientation from 'react-native-orientation-locker';
import { FullScreenVideo } from '@/src/components/atoms/FullScreenVideo';
import VideoPlayer from 'react-native-video-controls';
import ResizeIcon from '@/src/assets/icons/Resize.png'

const { width } = Dimensions.get('window');
const imW = width * 0.9;

const Wrapper = styled.View`
    justify-content: center;
    align-items: center;
    flex: 1;
    width: ${imW}px;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
`
const StyledView = styled.View`
    justify-content: center;
    flex: 1;
    align-self: center;
    border-radius: 4px;
    width: 100%;
    height: 245px;
`
const BottomVideoWrapper = styled.View`
flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-bottom-left-radius : 6px;
    border-bottom-right-radius : 6px;
    background: ${({ theme }) => theme.videoBackground};
    padding: 10px;
`
const Title = styled.Text`
    font-size: 12px;
    text-transform: uppercase;
    letter-spacing: 0.5px;
    font-size: 10px;
    line-height: 12px;
    color: #8B8787;
`

const ImageStyle = styled.Image`
    tint-color: #8B8787;
`
const ImagePress = styled.TouchableOpacity`
    padding: 5px;
`

interface props {
    url: string,
    audioOnly?: boolean,
}

export const CustomVideoPlayer: React.FC<props> = ({ url, audioOnly = false }) => {
    const [fullScreen, setFullScreen] = useState<boolean>(false)
    let player = useRef(null)

    function handleOrientation(orientation: string) {
        orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT'
            ? (setFullScreen(true), StatusBar.setHidden(true))
            : (setFullScreen(false),
                StatusBar.setHidden(false));
    }

    useEffect(() => {
        Orientation.addOrientationListener(handleOrientation);
        return () => {
            Orientation.removeOrientationListener(handleOrientation);
        };
    }, []);


    const handleFullScreen = () => {
        fullScreen ? Orientation.unlockAllOrientations() : Orientation.lockToLandscapeLeft();
    }

    return (
        <Wrapper>
            {fullScreen && <FullScreenVideo fullScreen={fullScreen} isVisible={fullScreen} setFullScreen={setFullScreen} setModalVisible={setFullScreen} uri={url} />}
            <StyledView>
                <VideoPlayer
                    audioOnly={audioOnly}
                    source={{
                        uri: url,
                    }}
                    toggleResizeModeOnFullscreen={false}
                    disableFullscreen={true}
                    disableBack={true}
                    disableVolume={true}
                    videoStyle={{ width: '100%', height: '100%' }}
                    style={{ width: '100%', height: '100%', borderTopLeftRadius: 6, borderTopRightRadius: 6 }}
                    paused
                    resizeMode='cover'
                    playWhenInactive={false}
                    ref={player}
                />
                <BottomVideoWrapper>
                    <Title>movie title</Title>
                    <ImagePress onPress={handleFullScreen}><ImageStyle source={ResizeIcon} /></ImagePress>
                </BottomVideoWrapper>
            </StyledView>
        </Wrapper>
    )
}