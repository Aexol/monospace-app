import React from 'react'
import { useColorState } from "@/src/containers/colorTheme";
import { ThemeProvider } from 'styled-components';
import { ContentStateProvider } from '@/src/containers/content';
import { TokenStateProvider } from '@/src/containers/token';

export const ApplicationWrapper: React.FC = ({ children }) => {
    const { colors } = useColorState();

    return (
        <ThemeProvider theme={colors}>
            <TokenStateProvider>
                <ContentStateProvider>
                    {children}
                </ContentStateProvider>
            </TokenStateProvider>
        </ThemeProvider>
    );
};