import { useColorState } from '@/src/containers/colorTheme';
import React, { useEffect, useState } from 'react';
import { Dimensions, KeyboardAvoidingView, Platform } from 'react-native';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import Routes from '@/src/navigation/Routes';
import { useContentState } from '@/src/containers/content';
import { ButtonIconLight, SearchIcon } from '@/src/assets/icons';
import ButtonIconDark from '@/src/assets/icons/ButtonIconDark';

const height = Dimensions.get('window').height

const Wrapper = styled(KeyboardAvoidingView)`
    flex-direction: row;
    position: absolute;
    bottom: 40px;
    left: 0;
    right: 0;
    align-items:center;
    align-self: center;
    flex: 1;
    padding: 0 15px;
`

const InputContainer = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items:center;
    color: ${({ theme }) => theme.inputText};
    background: ${({ theme }) => theme.largeInputBackground};
    shadow-color: #000;
    shadow-offset: {
    width: 0;
    height: 4;
    };
    shadow-opacity: 0.43;
    shadow-radius: 9.51px;
    elevation: 15;
    border-radius: 16px;
    height: ${height < 600 ? 50 : 56}px;
    flex: 1;
    padding: 0 15px 0 20px;
`
const ButtonWrapper = styled.TouchableOpacity`  
    margin-left : 10px;
    padding-top: 7px;
    justify-content: center;
    align-items: center;
`
const InputWrapper = styled.View`
    flex-direction: row;
    align-self: center;
    justify-content: space-between;
`
const TextStyle = styled.Text`
    align-self: center;
    font-family: Roboto-Regular;
    font-size: ${height < 600 ? 7 : 9}px;
    line-height: 11px;
    letter-spacing: 1px;
    padding-top: 4px;
    color: ${({ theme }) => theme.inputText};
`

const StyledTouchable = styled.TouchableOpacity`
    align-items: center;
    justify-content: center;
    padding-horizontal: 5px;
`
const TextInputStyle = styled.TextInput`
    padding-vertical: 10px;
    flex: 1;
    height: 100%;
    font-size: ${height < 600 ? 12 : 14}px;
    letter-spacing: 0.3px;
`

export const SearchInput: React.FC = () => {
    const { colorMode } = useColorState();
    const nav = useNavigation()
    const [searchedValue, setSearchedValue] = useState<string>('')
    const { userArticles, setFilterMode, setFilteredContent } = useContentState()


    useEffect(() => {
        if (searchedValue.length > 0) {
            setFilterMode(true)
            if (typeof userArticles !== 'string') {
                const dataFiltered = userArticles.filter(el => el.content.content.includes(searchedValue))
                setFilteredContent(dataFiltered)
            }
        }
        else if (searchedValue.length === 0) {
            setFilterMode(false)
        }
    }, [searchedValue])

    return (
        <Wrapper behavior={Platform.OS === 'ios' ? 'padding' : undefined} keyboardVerticalOffset={30}>
            <InputContainer>
                <InputWrapper>
                    <TextInputStyle placeholder='SEARCH' onChangeText={(t) => setSearchedValue(t)} value={searchedValue} defaultValue='' />
                    {searchedValue.length === 0 && <StyledTouchable>
                        <SearchIcon />
                    </StyledTouchable>}
                </InputWrapper>
            </InputContainer>
            <ButtonWrapper onPress={() => nav.navigate(Routes.NEW_NOTE)}>
                {colorMode === 'light' ? <ButtonIconLight /> : <ButtonIconDark />}
                <TextStyle>ADD NEW</TextStyle>
            </ButtonWrapper>
        </Wrapper>
    )
}
