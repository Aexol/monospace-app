import { useColorState } from '@/src/containers/colorTheme';
import React, { useState } from 'react';
import styled from 'styled-components/native'
import { Switch } from 'react-native-switch';
import { useEffect } from 'react';
import { StyleSheet } from 'react-native';

const CircleView = styled.View`
    width: 30px;
    height: 30px;
    border-radius: 30px;
    justify-content: center;
    align-items: center;
`
const TextStyle = styled.Text`
    font-size: 7px;
    text-align: center;
    color: ${({ theme }) => theme.text};
    text-align: center;
`

export const SwitchCustom: React.FC = () => {
    const { setColorMode, colorMode } = useColorState()
    const [isEnabled, setIsEnabled] = useState(false);

    useEffect(() => {
        colorMode === 'light' ? setIsEnabled(true) : setIsEnabled(false)
    }, [colorMode])

    const toggleSwitch = () => {
        if (colorMode === 'light') {
            setColorMode('dark')
        } else {
            setColorMode('light')
        }
    };

    return (
        <Switch
            value={isEnabled}
            onValueChange={toggleSwitch}
            disabled={false}
            activeText={'Light'}
            inActiveText={'Dark'}
            circleSize={24}
            barHeight={30}
            circleBorderWidth={0}
            backgroundActive={'#F2F2F2'}
            backgroundInactive={'#555555'}
            circleActiveColor={'#FFFFFF'}
            circleInActiveColor={'#414141'}
            renderInsideCircle={() => <CircleView><TextStyle>{isEnabled ? 'light' : 'dark'}</TextStyle></CircleView>}
            changeValueImmediately={true}
            innerCircleStyle={styles.innerCircleStyle}
            renderActiveText={false}
            renderInActiveText={false}
            switchLeftPx={3}
            switchRightPx={3}
            switchWidthMultiplier={2}
            switchBorderRadius={20}
        />
    )
}

const styles = StyleSheet.create({
    innerCircleStyle: {
        alignItems: "center",
        justifyContent: "center",
        margin: 3
    }
})
