import React from 'react';
import styled, { useTheme } from 'styled-components/native';
import { scale } from 'react-native-size-matters';
import { useTokenState } from '@/src/containers/token';
import { Dimensions } from 'react-native';
import { Logout } from '@/src/assets/icons';

const width = Dimensions.get('window').width;

const LogoutWrapper = styled.TouchableOpacity`
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    width: ${scale(112)}px;
    height: 32px;
    border-radius: 8px;
    border-color: ${({ theme }) => theme.logoutBorder};
    border-width: 1px;
    padding: 2px;
`;

const StyledText = styled.Text`
    font-size: ${width <= 320 ? 12 : 14}px;
    font-family: Roboto-Light;
    color: ${({ theme }) => theme.logoutText};
    padding-left: 5px;
`

export const ButtonLogout: React.FC = () => {
    const { logout } = useTokenState();
    const theme = useTheme()

    return (
        <LogoutWrapper onPress={logout}>
            <Logout fill={theme.logoutText} stroke={theme.logoutText} />
            <StyledText>LOG OUT</StyledText>
        </LogoutWrapper>
    )
}