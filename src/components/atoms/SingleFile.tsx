import React from 'react'
import { CustomVideoPlayer } from '@/src/components/atoms/CustomVideoPlayer';
import styled from 'styled-components/native';
import { CustomPdfComponent, SinglePostImage } from '@/src/components/atoms';

const Wrapper = styled.View`
    width: 100%;
    align-self: center;
    flex-direction: row;
    flex-wrap: wrap;
    flex-basis: 100%;
    justify-content: center;
    align-items: center;
`

export const SingleFile: React.FC<{ el: { type: string, getUrl: string } }> = ({ el }) => {

    return (
        <Wrapper>
            {(el.type === 'image/jpeg' || el.type === 'image/png') && <SinglePostImage key={el.getUrl} uri={el.getUrl} />}
            {(el.type === 'video/mp4') && <CustomVideoPlayer url={el.getUrl} />}
            {(el.type === 'audio/aac' || el.type === "audio/mpeg" || el.type === 'audio/oga' || el.type === 'audio/mp3' || el.type === 'application/ogg') && <CustomVideoPlayer url={el.getUrl} audioOnly={true} />}
            {(el.type === 'application/pdf') && <CustomPdfComponent url={el.getUrl} />}
        </Wrapper>
    )
}