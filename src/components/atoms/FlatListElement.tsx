import React, { useEffect, useState } from 'react';
import { Dimensions } from "react-native";
import styled from "styled-components/native";
import LinkPreview from 'react-native-link-preview';
import moment from 'moment';
import { CustomYouTubePlayer, SingleFile } from '@/src/components/atoms';


const { height } = Dimensions.get('window');

const ItemWrapper = styled.View`
    width: 100%;
    flex-basis: 100%;
    margin-bottom: 20px;
`
const DateText = styled.Text`
    flex: 1;
    padding: 5px 0;
    color: ${({ theme }) => theme.dateText};
    flex-wrap: wrap;
    text-align: justify;
    font-size: ${height < 600 ? 12 : 14}px;
    line-height: 18px;
`
const ItemText = styled.Text`
    flex: 1;
    padding: 10px 0;
    color: ${({ theme }) => theme.text};
    flex-wrap: wrap;
    text-align: justify;
    font-size: ${height < 600 ? 12 : 14}px;
    line-height: ${height < 600 ? 16 : 18}px;
`

interface SingleItem {
    content: {
        content: string;
        files?: {
            type: string;
            getUrl: string;
        }[] | undefined;
    };
}

interface ImageFile {
    type: string;
    uri: string;
    height?: number
}

export const FlatListElement: React.FC<{ item: SingleItem, createdAt: string }> = ({ item, createdAt }) => {
    const [videoId, setVideoId] = useState<string>();

    useEffect(() => {
        const getLink = async () => {
            if (item && item.content && (item.content.content.includes('https://youtu.be' || 'http://www.youtube.com'))) {
                const data = await LinkPreview.getPreview(item.content.content)
                if (data) {
                    let link = data.url;
                    const videoId = youtubeParse(link);
                    if (videoId !== false) {
                        setVideoId(videoId)
                    }
                }
            }
        }
        getLink()
    }, [item])

    const youtubeParse = (url: string) => {
        const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
        const match = url.match(regExp);
        return (match && match[7].length == 11) ? match[7] : false;
    }

    return (
        <ItemWrapper>
            <DateText>
                {moment(createdAt).format("MMM DD")}
            </DateText>
            <ItemText>{item.content.content}</ItemText>
            {item.content.files && item.content.files.map((el, index) => <SingleFile el={el} key={index} />)}
            {videoId && <CustomYouTubePlayer videoId={videoId} />}
        </ItemWrapper>
    )
}