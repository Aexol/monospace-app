import React from "react";
import { TextInputProps, View } from 'react-native'
import { scale } from "react-native-size-matters";
import styled, { useTheme } from "styled-components/native";

const StyledPhoneInput = styled.TextInput`
    background: ${({ theme }) => theme.inputBackground};
    color: ${({ theme }) => theme.inputText};
    border-radius: 8px;
    width: ${scale(264)}px;
    height: 48px;
    font-size: 14px;
    letter-spacing: 0.4px;
    padding-left: 20px;
    font-family: Roboto-Bold;
    letter-spacing: 0.3px;
    align-items: center;
    justify-content: center;
`

const InputLabel = styled.Text`
  font-size: 12px;
  letter-spacing: 0.2px;
  padding: 0 0 5px 15px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.inputText};
`;

interface props extends TextInputProps {
    label: string;
    value: string;
}

export const CustomPhoneInput: React.FC<props> = ({ label, value, onChangeText }) => {
    const theme = useTheme()

    return (
        <View>
            <InputLabel>{label}</InputLabel>
            <StyledPhoneInput
                placeholderTextColor={theme.inputText}
                keyboardType='number-pad'
                value={value}
                onChangeText={onChangeText}
                placeholder='phone number'
            />
        </View>
    );
};