import React, { useState } from 'react'
import styled from 'styled-components/native'
import { PDFViewer } from '@/src/components/atoms/PDFViewer';
import { PermissionsAndroid, Platform } from 'react-native';
import { PdfIcon } from '@/src/assets/icons/PdfIcon';

const Wrapper = styled.View`
    flex: 1;
`
const StyledText = styled.Text`
    font-family: Roboto-Light;
    font-size: 14px;
    line-height: 16px;
    color: ${({ theme }) => theme.inputText};
    padding-left: 10px;
`

const StyledPdfImageWrapper = styled.TouchableOpacity`
    padding: 10px;
    align-self: flex-start;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
`
interface props {
    url: string
}

export const CustomPdfComponent: React.FC<props> = ({ url }) => {

    const [isPDFOpen, setIsPDFOpen] = useState<boolean>(false)

    async function requestStoragePermission() {

        if (Platform.OS !== "android") return true

        const pm1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
        const pm2 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);

        if (pm1 && pm2) return true

        const userResponse = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
        ]);

        if (userResponse['android.permission.READ_EXTERNAL_STORAGE'] === 'granted' &&
            userResponse['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
            return true
        } else {
            return false
        }
    }

    return (
        <Wrapper>
            {isPDFOpen && <PDFViewer url={url} closePdf={() => setIsPDFOpen(false)} />}
            {!isPDFOpen && <StyledPdfImageWrapper onPress={() => { requestStoragePermission(); setIsPDFOpen(true) }}><PdfIcon /><StyledText>PDF title</StyledText></StyledPdfImageWrapper>}
        </Wrapper>
    )
}