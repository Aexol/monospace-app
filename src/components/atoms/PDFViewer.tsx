import React from 'react';
import { Dimensions } from 'react-native';
import Pdf from 'react-native-pdf';
import styled from 'styled-components/native';

const PDFWrapper = styled.View`
    flex: 1;
    justify-content: flex-start;
    align-items: center;
    background-color: orange;
    position: relative;
`
const ClosePdf = styled.TouchableOpacity`
    position: absolute;
    right: 20px;
    top: 0px;
    z-index: 1;
    justify-content: center;
    align-items: center;
`
const StyledX = styled.Text`
    color: black;
    padding: 3px;
    font-size: 12px;
    align-self: center;
    text-transform: uppercase;
`

const StyledPdf = styled(Pdf)`
    flex: 1;
    width: ${Dimensions.get('window').width}px;
    height: 500px;
    background-color: white;
`

export class PDFViewer extends React.Component<{ url: string, closePdf: () => void }> {
    render() {
        const source = { uri: this.props.url, cache: false };

        return (
            <PDFWrapper>
                <ClosePdf onPress={this.props.closePdf}><StyledX>Zamknij</StyledX></ClosePdf>
                <StyledPdf source={source} />
            </PDFWrapper>
        )
    }
}