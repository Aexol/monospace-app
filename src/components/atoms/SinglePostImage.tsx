import React, { useEffect, useState } from 'react';
import { Image, Dimensions } from 'react-native';
import styled from 'styled-components/native';

const { width } = Dimensions.get('window');
const imH = Math.round(width * 10 / 16);

const StyledImage = styled.Image<{ w: number, h: number }>`
    width: ${(props) => props.w}px;
    height: ${(props) => props.h}px;
    border-radius: 6px;
    margin-bottom: 10px;
`;

interface props {
    uri: string
}

export const SinglePostImage: React.FC<props> = ({ uri }) => {
    const [imageWidth, setImageWidth] = useState<number>(0)
    const [imageHeight, setImageHeight] = useState<number>(0)

    useEffect(() => {
        Image.getSize(uri, (widthDB, heightDB) => {
            if (widthDB > heightDB) {
                setImageHeight(imH)
                setImageWidth(width * 0.96)
            }
            else if (heightDB > widthDB) {
                setImageWidth(0.45 * width)
                setImageHeight(width * 10 / 16)
            }
            else {
                setImageWidth(width)
                setImageHeight(imH)
            }
        })
    }, [])

    return (
        <StyledImage w={imageWidth} h={imageHeight} source={{ uri: uri }} />
    )
}