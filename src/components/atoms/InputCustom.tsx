import React from 'react';
import { TextInputProps } from 'react-native';
import { scale } from 'react-native-size-matters';
import styled, { useTheme } from 'styled-components/native';

const InputContainer = styled.View`
  position: relative;
  text-align: center;
`;

const InputStyles = styled.TextInput`
  background: ${({ theme }) => theme.inputBackground};
  color: ${({ theme }) => theme.inputText};
  border-radius: 8px;
  width: ${scale(264)}px;
  height: 48px;
  font-size: 14px;
  letter-spacing: 0.4px;
  padding: 16px 20px;
  font-family: Roboto-Bold;
  letter-spacing: 0.3px;
  align-items: center;
`;

const InputLabel = styled.Text`
  font-size: 12px;
  letter-spacing: 0.2px;
  padding: 0 0 5px 15px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.inputText};
`;

const ErrorText = styled.Text`
  color: red;
  text-align: center;
  padding-right: 8px;
  font-size: 12px;
  padding-bottom: 15px;
`;
interface InputProps extends TextInputProps {
  label: string;
  errorText?: string;
  placeholder?: string;
  placeholderTextColor?: string;
}

export const InputCustom: React.FC<InputProps> = ({
  onChangeText,
  value,
  label,
  keyboardType,
  onFocus,
  onBlur,
  errorText,
  placeholderTextColor,
  placeholder,
  onKeyPress,
  defaultValue,
  maxLength
}) => {
  const theme = useTheme()
  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <InputStyles
        selectionColor={theme.inputText}
        defaultValue={defaultValue}
        multiline={true}
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        onFocus={onFocus}
        onBlur={onBlur}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        onKeyPress={onKeyPress}
        maxLength={maxLength}
      />
      {errorText ? <ErrorText>{errorText}</ErrorText> : <ErrorText />}
    </InputContainer>
  );
};
