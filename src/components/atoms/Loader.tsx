import React from 'react';
import { ActivityIndicator, ActivityIndicatorProps } from 'react-native';
import styled from 'styled-components/native';

const LoadingView = styled.View`
  background-color: white;
  justify-content: center;
  align-items: center;
  flex: 1;
`;

export const Loader: React.FC<ActivityIndicatorProps> = () => {
    return (
        <LoadingView>
            <ActivityIndicator size="large" color={'gray'} />
        </LoadingView>
    );
};