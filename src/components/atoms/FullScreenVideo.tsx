import React, { Dispatch, SetStateAction } from 'react'
import { Dimensions, Modal, View } from 'react-native';
import styled from 'styled-components/native';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation-locker';

const ModalWrapper = styled.View`
    flex: 1;
`
interface props {
    isVisible: boolean,
    uri: string,
    setModalVisible: Dispatch<SetStateAction<boolean>>,
    setFullScreen: Dispatch<SetStateAction<boolean>>,
    fullScreen: boolean
}

export const FullScreenVideo: React.FC<props> = ({ isVisible, uri, setModalVisible, setFullScreen, fullScreen }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'red' }}>
            <Modal
                animationType={'fade'}
                supportedOrientations={['landscape']}
                transparent={true}
                visible={isVisible}
                statusBarTranslucent={true}
                presentationStyle={'overFullScreen'}>
                <ModalWrapper>
                    <VideoPlayer
                        source={{
                            uri: uri,
                        }}
                        paused
                        onBack={() => { setModalVisible(false); setFullScreen(false); Orientation.lockToPortrait() }}
                        resizeMode="contain"
                        toggleResizeModeOnFullscreen={false}
                        disableFullscreen={true}
                        onEnterFullscreen={() => {
                            setFullScreen(true);
                            setModalVisible(true)
                        }}
                        style={{ flex: 1 }}
                    />
                </ModalWrapper>
            </Modal>
        </View>
    )
}