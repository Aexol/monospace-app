import React from 'react';
import styled from 'styled-components/native';
import { TouchableOpacityProps } from 'react-native';
import { scale } from 'react-native-size-matters';

const CustomTouchable = styled.TouchableOpacity`
    background: ${({ theme }) => theme.buttonBackground};
    border-radius: 8px;
    padding-vertical: 10px;
    width: ${scale(112)}px;
    align-self: center;
    justify-content: center;
    align-items: center;
`
const ButtonText = styled.Text`
    font-family: Roboto-Regular;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
    letter-spacing: 0.3px;
    color: ${({ theme }) => theme.buttonText};
    text-transform: uppercase;
`;

interface Props extends TouchableOpacityProps {
    onPress?(t: any): void;
    name: string;
}

export const ButtonCustom: React.FC<Props> = ({
    name,
    onPress,
    disabled
}) => {

    return (
        <CustomTouchable disabled={disabled} onPress={onPress} >
            <ButtonText>{name}</ButtonText>
        </CustomTouchable>
    );
};
