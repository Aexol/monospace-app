import { useColorState } from '@/src/containers/colorTheme';
import React from 'react';
import { Dimensions, Platform, StatusBar } from 'react-native';
import styled from 'styled-components/native';

const SafeArea = styled.SafeAreaView`
  flex: 1;
  background: ${({ theme }) => theme.background};
  justify-content: center;
  width: ${Dimensions.get('window').width}px;
`;
const ScreenContainer = styled.View`
  flex: 1;
  justify-content: space-around;
  align-items: center;
  color: ${({ theme }) => theme.text};
  width: ${Dimensions.get('window').width}px;
  padding: 12px;
`;

export const Container: React.FC = ({ children }) => {

  const { colorMode } = useColorState()

  return (
    <>
      {Platform.OS === 'ios' && <StatusBar barStyle={colorMode === 'dark' ? "dark-content" : 'light-content'} />}
      <SafeArea>
        <ScreenContainer>{children}</ScreenContainer>
      </SafeArea>
    </>
  );
};
