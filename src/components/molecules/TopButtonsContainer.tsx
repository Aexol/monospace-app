import { ButtonLogout, SwitchCustom } from '@/src/components/atoms';
import React from 'react';
import styled from 'styled-components/native';

const TopContainer = styled.View`
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    padding-bottom: 15px;
`

export const TopButtonsContainer = () => {
    return (
        <TopContainer>
            <ButtonLogout />
            <SwitchCustom />
        </TopContainer>
    )
}