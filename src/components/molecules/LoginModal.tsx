import { ButtonCustom } from "@/src/components/atoms";
import { InputCustom } from "@/src/components/atoms/InputCustom";
import React, { useState } from "react";
import { ActivityIndicator, Modal, Keyboard, StyleSheet, Dimensions, KeyboardAvoidingView, Platform, TouchableWithoutFeedback } from "react-native";
import { scale, verticalScale } from 'react-native-size-matters';
import styled, { useTheme } from "styled-components/native";
import { useTokenState } from "@/src/containers/token";
import { useBackend } from "@/src/hooks/backend";
import PhoneInput from "react-native-phone-number-input";

const height = Dimensions.get('window').height;

const InputsWrapper = styled(KeyboardAvoidingView)`
    align-self: center;
    justify-content: space-around;
    margin-top: 0;
    flex: 0.85;
`
const CodeInputWrapper = styled(KeyboardAvoidingView)`
    align-self: center;
    justify-content: space-around;
`

const ModalContainer = styled.View`
    flex: 1; 
    background: ${({ theme }) => theme.background};
    justify-content: center; 
    align-items: center;
`

const ModalStyles = styled.View`
    height: ${height > 570 ? verticalScale(416) : verticalScale(520)}px;
    width: ${scale(296)}px;
    background: ${({ theme }) => theme.modalBackground};
    border-radius: 8px;
    align-items: center;
    justify-content: space-around;
    shadow-color: #000;
    shadow-offset: {
    width: 0;
    height: 2;
    };
    shadow-opacity: 0.25;
    shadow-radius: 4px;
    elevation: 5;
    padding: 20px;
`;

const ModalText = styled.Text`
    font-size: 16px;
    line-height: 19px;
    color: ${({ theme }) => theme.logoutText};
`;

const CloseTouchable = styled.Text`
    font-size: 20px;
    line-height: 22px;
    color: #C4C4C4;
    align-self: flex-end;
    position: absolute;
    top: 10px;
    right: 15px;
    font-family: Roboto-Light;
    padding: 10px;
`;

const InputLabel = styled.Text`
  font-size: 12px;
  letter-spacing: 0.2px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.inputText};
  padding: 0 0 5px 15px;
`;

const ErrorText = styled.Text`
  font-size: 12px;
  letter-spacing: 0.2px;
  color: red;
  text-align: center;
`;

const InputError = styled.Text`
  font-size: 12px;
  letter-spacing: 0.2px;
  color: red;
  padding: 10px 0 0 10px;
`;

const InputContainer = styled.View`
  position: relative;
  text-align: center;
`;

const InputWrapper = styled.View`
  flex: 0.6;
  justify-content: flex-end;
  align-items: center;
`;

interface Props {
    modalVisible: boolean;
    closeModal: () => void
}

export const LoginModal: React.FC<Props> = ({ modalVisible, closeModal }) => {
    const theme = useTheme()
    const { setUserName, setPhone, phone, setIsAuthenticated, setToken } = useTokenState()
    const { validateUserLogin, login } = useBackend()
    const [awaitOTP, setAwaitOTP] = useState<boolean>(false)
    const [loginMode, setLoginMode] = useState<boolean>(true)
    const [loading, setLoading] = useState<boolean>(false)
    const [invalidOTP, setInvalidOTP] = useState<string>('')
    const [incorrectPhone, setIncorrectPhone] = useState<string>('')
    const [error, setError] = useState<string>('')
    const [name, setName] = useState<string>();
    const [phoneNr, setPhoneNr] = useState<string>('');
    const [code, setCode] = useState<string>('');
    const [formattedValue, setFormattedValue] = useState<string>("");
    const [disableAfterPress, setDisableAfterPress] = useState<boolean>(false);

    const validateName = (t: string) => {
        setName(t)
        phoneNr && setError('')
    }

    const validatePhoneNr = (text: string) => {
        if (text.length < 9) {
            setIncorrectPhone('Phone number is too short.');
        } else {
            setIncorrectPhone('')
            setPhoneNr(text)
            if (name) {
                setError('')
            }
        }
    };

    const handleSubmit = async () => {
        if (name && formattedValue && error === '' && incorrectPhone === '') {
            setDisableAfterPress(true);
            setTimeout(() => {
                setDisableAfterPress(false);
            }, 10000);
            setUserName(name)
            setPhone(formattedValue)
            setLoading(true)
            login(formattedValue, name)
            setAwaitOTP(true)
            setLoginMode(false)
            setLoading(false)
        }
        else {
            setError('All fields are required')
        }
    }

    const handleValidate = async () => {
        if (code && phone) {
            setError('')
            const res = await validateUserLogin(code, phone)
            if (res) {
                if (typeof res === 'string' && res === 'Invalid OTP') {
                    setInvalidOTP('Code is incorrect')
                }
                if (typeof res === 'string' && res === 'Undefined Error') {
                    setInvalidOTP(`Something wend wrong. \nPlease try to get the code again.`)
                }
                else {
                    setToken(res)
                    setIsAuthenticated(true)
                }
            }
            else {
                setInvalidOTP('Code is incorrect')
            }
        }
        else {
            setError('All fields are required.')
        }
    }

    return (
        <Modal animationType="fade" transparent={true} visible={modalVisible} onRequestClose={() => closeModal()}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}
                accessible={false}>
                <ModalContainer>
                    <ModalStyles>
                        <CloseTouchable onPress={() => closeModal()}>x</CloseTouchable>
                        <ModalText>Please sign up</ModalText>
                        {loginMode &&
                            <InputsWrapper behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
                                <InputCustom
                                    label='* USERNAME'
                                    defaultValue={''}
                                    value={name}
                                    onChangeText={(t) => validateName(t)} placeholderTextColor={theme.inputText}
                                />
                                <InputContainer>
                                    <InputLabel>* PHONE NUMBER</InputLabel>
                                    <PhoneInput
                                        value={phoneNr}
                                        defaultCode="PL"
                                        onChangeText={(text) => {
                                            validatePhoneNr(text);
                                        }}
                                        onChangeFormattedText={(text) => {
                                            setFormattedValue(text);
                                        }}
                                        textInputProps={{
                                            inlineImagePadding: 0,
                                            keyboardType: "number-pad",
                                            maxLength: 14,
                                            placeholder: '',
                                            multiline: true,
                                            selectionColor: theme.inputText,
                                        }}
                                        countryPickerButtonStyle={stylesPhoneInput.countryPickerButtonStyle}
                                        textInputStyle={[stylesPhoneInput.textInputStyle, { color: theme.inputText }]}
                                        codeTextStyle={[stylesPhoneInput.codeTextStyle, { color: theme.inputText }]}
                                        containerStyle={[{ backgroundColor: theme.inputBackground }, stylesPhoneInput.containerStyle]}
                                        textContainerStyle={[{ backgroundColor: theme.inputBackground }, stylesPhoneInput.textContainerStyle]}
                                        disableArrowIcon={true}
                                        placeholder=''
                                    />
                                    <InputError>{incorrectPhone}</InputError>
                                </InputContainer>
                                <ErrorText>{error}</ErrorText>
                                {loading ?
                                    <ActivityIndicator
                                        size="large"
                                        animating={true}
                                        color="#a9a9a3"
                                    /> :
                                    <ButtonCustom disabled={disableAfterPress} name={'Sign in'} onPress={() => handleSubmit()} />}
                            </InputsWrapper>
                        }
                        {awaitOTP &&
                            <CodeInputWrapper behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
                                <InputWrapper>
                                    <InputCustom maxLength={4} label='*CODE' keyboardType={"numeric"} defaultValue={''} value={code} onChangeText={(t) => setCode(t)} />
                                </InputWrapper>
                                <ErrorText>{invalidOTP}</ErrorText>
                                <ButtonCustom name={'Validate'} onPress={() => handleValidate()} />
                            </CodeInputWrapper>
                        }
                    </ModalStyles>
                </ModalContainer>
            </TouchableWithoutFeedback>
        </Modal>
    );
};

const stylesPhoneInput = StyleSheet.create({
    textInputStyle: {
        fontSize: 13,
        fontFamily: 'Roboto-Regular',
        letterSpacing: 0.3,
        padding: 0,
        paddingBottom: Platform.OS === 'android' ? 0 : 3,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    countryPickerButtonStyle: {
        width: 50,
        paddingLeft: 20,
        paddingRight: 0,
    },
    codeTextStyle: {
        fontSize: 13,
        letterSpacing: 0.3,
        height: 48,
        lineHeight: 48,
        fontFamily: 'Roboto-Regular',
    },
    containerStyle: {
        width: scale(264),
        borderRadius: 8,
        height: 48,
    },
    textContainerStyle: {
        height: 48,
        borderTopRightRadius: 8,
        borderBottomRightRadius: 8,
        lineHeight: 48,
        fontFamily: 'Roboto-Regular',
    }
})
