import React from 'react';
import { StyleSheet } from 'react-native';
import { ZeusHook } from '@/src/zeus';
import { useBackend } from '@/src/hooks/backend';
import { FlatListElement } from '@/src/components/atoms/FlatListElement';
import { OptimizedFlatList } from 'react-native-optimized-flatlist'
import styled from 'styled-components/native';

const StyledSafeAreaView = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
`;
interface ItemsContainerProps {
    items: ZeusHook<typeof useBackend, 'getWall'>;
}
interface SingleItem {
    createdAt: string;
    content: {
        content: string;
        files?: {
            type: string;
            getUrl: string;
        }[] | undefined;
    };
}

export const ItemsContainer: React.FC<ItemsContainerProps> = ({ items }) => {

    const renderItem = ({ item, index }: { item: SingleItem, index: number }) => {
        return (
            <FlatListElement key={index} item={item} createdAt={item.createdAt} />
        )
    }

    return (
        <StyledSafeAreaView>
            <OptimizedFlatList
                keyExtractor={(item: SingleItem, index: number) => index}
                contentContainerStyle={styles.contentContainerStyle}
                initialNumToRender={6}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                data={typeof items !== 'string' ? items : null}
                maxToRenderPerBatch={6}
            />
        </StyledSafeAreaView>
    );
};

const styles = StyleSheet.create({
    contentContainerStyle: {
        justifyContent: 'center',
        width: '100%',
        paddingBottom: 80,
    }
})

