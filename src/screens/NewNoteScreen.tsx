import React, { useState } from 'react';
import { Alert, PermissionsAndroid, Platform, TouchableOpacity } from 'react-native';
import styled, { useTheme } from 'styled-components/native';
import DocumentPicker, { DocumentPickerResponse } from 'react-native-document-picker';

import { ButtonCustom, Loader } from '@/src/components/atoms';
import { Container } from '@/src/components/molecules';
import { AddNew, SendLink } from '@/src/assets/icons'
import { useContentState } from '@/src/containers/content';
import { useBackend } from '@/src/hooks/backend';
import { useNavigation } from '@react-navigation/native';
import Routes from '@/src/navigation/Routes';
import { TopButtonsContainer } from '@/src/components/molecules/TopButtonsContainer';
import RNFetchBlob from 'rn-fetch-blob';

const BottomContainer = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    padding: 0 5px;
`

const BreadStyle = styled.TextInput`
    width: 100%;
    flex: 0.9;
    background: ${({ theme }) => theme.newNoteDominance};
    color: ${({ theme }) => theme.newNoteText};
    padding: 25px;
    font-size: 16px;
    line-height: 19px;
    font-family: Roboto-Regular;
    letter-spacing: 0.3px;
    border-radius: 16px;
`

export const NewNoteScreen: React.FC = () => {
    const theme = useTheme()
    const { content, setContent, setFileSelected } = useContentState();
    const { postMutationWithoutFile, postMutationWithFile, uploadFiles } = useBackend();
    const [multipleFile, setMultipleFile] = useState<DocumentPickerResponse[]>([]);
    const [isFileUploaded, setIsFileUploaded] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const nav = useNavigation()
    var http = new XMLHttpRequest();

    async function requestStoragePermission() {

        if (Platform.OS !== "android") return true

        const pm1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
        const pm2 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);

        if (pm1 && pm2) return true

        const userResponse = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
        ]);

        if (userResponse['android.permission.READ_EXTERNAL_STORAGE'] === 'granted' &&
            userResponse['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
            return true
        } else {
            return false
        }
    }

    const selectMultipleFiles = async () => {
        const res = await requestStoragePermission()
        if (!res) {
            return;
        }
        try {
            const results = await DocumentPicker.pickMultiple({
                allowMultiSelection: true,
                type: [DocumentPicker.types.allFiles],
            });
            for (const res of results) {
                setMultipleFile(results)
                setIsFileUploaded(true)
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                Alert.alert('No file selected.');
            } else {
                Alert.alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }
    const getPath = async (uri: string) => {
        if (uri.startsWith('content://')) {
            return RNFetchBlob.fs.stat(uri).then(info => info?.path);
        }
        return uri;
    };

    const handleContentSubmit = async () => {
        if (content === '') {
            return Alert.alert("You can not add an empty post.")
        }
        if (isFileUploaded && multipleFile && content) {
            const uploadResponseRes = await uploadFiles(
                multipleFile.map(file => {
                    return {
                        name: file.name,
                        type: file.type,
                    }
                })
            )
            if (uploadResponseRes) {
                uploadResponseRes.forEach(async (file, index) => {
                    const convertedUri = await getPath(multipleFile[index].uri)
                    let fileToUpload = {
                        uri: Platform.OS === 'android' ? "file://" + convertedUri : convertedUri,
                        type: multipleFile[index].type,
                        name: multipleFile[index].name,
                    }

                    fetch(file.putUrl, {
                        headers: {
                            'Content-Type': fileToUpload.type,
                            'x-amz-acl': 'public-read'
                        },
                        method: "PUT",
                        body: fileToUpload,
                    }).then(resp => console.log(resp)).then(result => console.log("Success", result)).catch(err => console.log('error from fetch', err))

                })
                if (uploadResponseRes.length > 0) {
                    const response = await postMutationWithFile(content, uploadResponseRes.map((el, index) => {
                        return {
                            getUrl: el.getUrl,
                            type: multipleFile[index].type
                        }
                    }))
                    if (response) {
                        setLoading(false)
                        Alert.alert('Post has been added');
                        nav.navigate(Routes.WALL)
                    } else {
                        setLoading(false)
                        Alert.alert("Files could not be added, please try again.")
                    }
                }

            }

            else {
                Alert.alert("Fiel could not be uploaded. Please try again")
            }
        }
        else {
            setLoading(true)
            const res = await postMutationWithoutFile(content);
            if (res) {
                setLoading(false)
                Alert.alert('Post has been added');
                nav.navigate(Routes.WALL)
            }
            else {
                setLoading(false)
                Alert.alert("Sth went wrong with the article upload. Please try again.")
            }
        }
    }

    if (loading) {
        return <Loader />
    }

    return (
        <Container>
            <TopButtonsContainer />
            <BreadStyle
                style={{ textAlignVertical: 'top' }}
                multiline={true}
                placeholder='Type here...'
                placeholderTextColor={theme.newNoteText}
                value={content}
                onChangeText={(t) => setContent(t)}
            />
            <BottomContainer>
                <TouchableOpacity onPress={() => nav.navigate(Routes.WALL)}>
                    <SendLink />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => selectMultipleFiles()}>
                    <AddNew fill={multipleFile.length > 0 ? 'green' : '#C4C4C4'} />
                </TouchableOpacity>
                <ButtonCustom name='Publish' onPress={() => handleContentSubmit()} />
            </BottomContainer>
        </Container>
    )
};
