import { SearchInput } from '@/src/components/atoms';
import { Container } from '@/src/components/molecules';
import { ItemsContainer } from '@/src/components/molecules/ItemsContainer';
import { TopButtonsContainer } from '@/src/components/molecules/TopButtonsContainer';
import { useColorState } from '@/src/containers/colorTheme';
import { useContentState } from '@/src/containers/content';
import { useBackend } from '@/src/hooks/backend';
import { ZeusHook } from '@/src/zeus';
import { useFocusEffect } from '@react-navigation/native';
import React, { useState } from 'react';
import styled, { useTheme } from 'styled-components/native';
import { BlurView } from "@react-native-community/blur";
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const Logo = styled.Text`
    position: absolute;
    bottom: -33px;
    font-family: Roboto-Bold;
    font-size: ${width <= 320 ? 50 : 64}px;
    color: ${({ theme }) => theme.logoColor};
    width: 100%;
    text-align: center;
    letter-spacing: 1.2px;
    flex-wrap: nowrap;
`

const BlurViewStyles = styled(BlurView)`
    position: absolute;
    left: 0; 
    bottom: 0; 
    right: 0; 
    height: 110px; 
    opacity: 0.9;
`

const EmptyText = styled.Text`
    font-family: Roboto-Regular;
    font-size: 14px;
    line-height: 20px;
    text-align: center;
    letter-spacing: 0.3px;
    margin-top: 20px;
    flex: 1;
    color: ${({ theme }) => theme.text};
`
interface ArrayElement {
    createdAt: string,
    content: {
        content: string,
        files?: ImageFile[]
    }
}

interface ImageFile {
    type: string;
    getUrl: string;
}

export const WallScreen = () => {
    const [dataToDisplay, setDataToDisplay] = useState<ZeusHook<typeof useBackend, 'getWall'>>([]);
    const [empty, setEmpty] = useState<boolean>(false)
    const [dataError, setDataError] = useState<boolean>(false)
    const { getUserInfo, getUserByUserName } = useBackend()
    const { colorMode } = useColorState()
    const { filteredContent, setUserArticles, filterMode, userArticles } = useContentState()
    const theme = useTheme()


    useFocusEffect(
        React.useCallback(() => {
            const getUserWall = async () => {
                if (filterMode) {
                    setDataToDisplay(filteredContent)
                }
                else {
                    try {
                        const res = await getUserInfo();
                        if (res && typeof res === 'string' && res === 'Invalid Content') {
                            setDataError(true)
                        }
                        else {
                            const wallItems = res.wall.reverse()
                            wallItems.length === 0 ? setEmpty(true) : setEmpty(false)
                            let newArray: ArrayElement[] = []
                            wallItems.forEach((item, index) => {
                                if (item && item.content && !item.content.files) {
                                    let newObject: ArrayElement = {
                                        createdAt: wallItems[index].createdAt,
                                        content: {
                                            content: wallItems[index].content.content
                                        }
                                    }
                                    newArray.push(newObject)
                                }
                                else if (item.content.files && item.content.files?.length > 0) {
                                    let newObject: ArrayElement = {
                                        createdAt: wallItems[index].createdAt,
                                        content: {
                                            content: wallItems[index].content.content,
                                            files: wallItems[index].content.files,
                                        }
                                    }
                                    newArray.push(newObject)
                                }
                            })
                            setUserArticles(newArray)
                            setDataToDisplay(newArray)
                        }
                    } catch (err) {
                        return;
                    }
                }
            }
            getUserWall();
        }, [filteredContent, filterMode]),
    );

    return (
        <Container>
            <TopButtonsContainer />
            {dataError && <EmptyText>Data couldn't be loaded</EmptyText>}
            {empty ? <EmptyText>Your wall is empty...</EmptyText> : <ItemsContainer items={dataToDisplay} />}
            <BlurViewStyles
                blurType={colorMode === "dark" ? "dark" : 'xlight'}
                blurAmount={30}
                reducedTransparencyFallbackColor={theme.transparencyColor}
                overlayColor={theme.transparencyColor}
            />
            <SearchInput />
            <Logo numberOfLines={1}>monospace</Logo>
        </Container>
    )
};
