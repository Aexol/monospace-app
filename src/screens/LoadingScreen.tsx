import React from 'react';
import styled from 'styled-components/native';
import { useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import Routes from '@/src/navigation/Routes';
import { useTokenState } from '@/src/containers/token';

const Wrapper = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    background-color: white;
`

const StyledText = styled.Text`
    font-family: Roboto-Bold;
    font-size: 50px;
    color: ${({ theme }) => theme.logoColor};
    width: 100%;
    text-align: center;
    letter-spacing: 1.2px;
    flex-wrap: nowrap;
    padding-top: 20px;
`

export const LoadingScreen: React.FC = () => {
    const { loadingToken } = useTokenState()
    const nav = useNavigation()

    useEffect(() => {
        if (!loadingToken) {
            nav.navigate(Routes.START_SCREEN)
        }
    }, [])

    return (<>
        {loadingToken &&
            <Wrapper>
                <StyledText>monospace</StyledText>
            </Wrapper>}
    </>
    )
}