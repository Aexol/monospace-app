import React, { useState } from 'react';
import styled from 'styled-components/native';
import { Container } from '@/src/components/molecules';
import StartScreenIcon from '@/src/assets/images/StartScreen.png';
import { ButtonCustom } from '@/src/components/atoms';
import { LoginModal } from '@/src/components/molecules/LoginModal';
import { useColorState } from '@/src/containers/colorTheme';
import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width

const ButtonsContainer = styled.View`
    flex: 0.1;
    padding-bottom: 20px;
`
const ImageContainer = styled.View`
    flex: 0.35;
    justify-content: flex-end;
`
const StyledTextBold = styled.Text`
    font-family: Roboto-Bold;
    font-size: ${width > 320 ? 52 : 49}px;
    line-height: 66px;
    margin-bottom: 10px;
    flex: 0.15;
    color: ${({ theme }) => theme.logoColor};
`
const StyledText = styled.Text`
    font-family: Roboto-Light;
    font-size: 16px;
    line-height: 24px;
    letter-spacing: 0.3px;
    flex: 0.2;
    color: ${({ theme }) => theme.startScreenText};
    width: 100%;
    padding: 0 10px;
    text-align: center;
`
const StyledImage = styled.Image<{ dark: boolean }>`
    opacity: ${({ dark }) => dark ? 0.1 : 1};
`

export const StartScreen: React.FC = () => {
    const [loginModalVisible, setLoginModalVisible] = useState<boolean>(false);
    const { colorMode } = useColorState()

    return (
        <Container>
            {loginModalVisible &&
                <LoginModal modalVisible={loginModalVisible} closeModal={() => setLoginModalVisible(false)} />
            }
            <ImageContainer>
                <StyledImage source={StartScreenIcon} dark={colorMode === 'dark'} />
            </ImageContainer>
            <StyledTextBold>monospace</StyledTextBold>
            <StyledText>Welcome to monospace - place, where you can share your thoughts and feelings.</StyledText>
            <ButtonsContainer>
                <ButtonCustom name="Sign in" onPress={() => { setLoginModalVisible(true) }} />
            </ButtonsContainer>
        </Container>
    )
};
