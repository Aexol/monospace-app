import React from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import Routes from '@/src/navigation/Routes';
import { NewNoteScreen, WallScreen } from '@/src/screens';
import { Platform } from 'react-native';

const Stack = createStackNavigator();

const MainNavigator: React.FC = () => {
    return (
        <Stack.Navigator mode="modal" screenOptions={{ ...(Platform.OS === 'android' && TransitionPresets.FadeFromBottomAndroid) }}>
            <Stack.Screen
                name={Routes.WALL}
                component={WallScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={Routes.NEW_NOTE}
                component={NewNoteScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    )
}

export default MainNavigator;