import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Routes from '@/src/navigation/Routes';
import LoginNavigator from '@/src/navigation/LoginNavigator';
import MainNavigator from '@/src/navigation/MainNavigator';
import { useTokenState } from '@/src/containers/token';

const Stack = createStackNavigator();

export const AppNavigator: React.FC = () => {
    const { isAuthenticated } = useTokenState();

    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ ...(Platform.OS === 'android' && TransitionPresets.FadeFromBottomAndroid) }}>
                {!isAuthenticated ? (
                    <Stack.Screen
                        name={Routes.LOGIN_NAVIGATOR}
                        component={LoginNavigator}
                        options={{ headerShown: false }}
                    />
                ) : (
                    <Stack.Screen
                        name={Routes.MAIN_NAVIGATOR}
                        component={MainNavigator}
                        options={{ headerShown: false }}
                    />
                )}
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AppNavigator;
