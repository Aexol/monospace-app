import React from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { LoadingScreen, StartScreen } from '@/src/screens';
import Routes from '@/src/navigation/Routes';
import { Platform } from 'react-native';

const Stack = createStackNavigator();

const LoginNavigator: React.FC = () => {
    return (
        <Stack.Navigator initialRouteName={Routes.LOADING_SCREEN} screenOptions={{ ...(Platform.OS === 'android' && TransitionPresets.FadeFromBottomAndroid) }}>
            <Stack.Screen
                name={Routes.LOADING_SCREEN}
                component={LoadingScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name={Routes.START_SCREEN}
                component={StartScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    )
}

export default LoginNavigator;