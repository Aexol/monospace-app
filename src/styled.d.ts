import {EditorTheme} from './theme';
declare module 'styled-components' {
  export interface DefaultTheme extends EditorTheme {}
}
