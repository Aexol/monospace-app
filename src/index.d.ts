declare module '*.png';
declare module '*.svg';
declare module 'react-native-link-preview';
declare module 'react-native-video-players';
declare module 'react-native-optimized-flatlist';
declare module 'react-native-openanything';
declare module 'react-native-pdf-view';
declare module 'react-native-video-controls';