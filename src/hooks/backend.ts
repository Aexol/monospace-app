import { useTokenState } from '@/src/containers/token';
import {Chain, GraphQLError} from '../zeus/index';

export const useBackend = () => {
  const {token, isAuthenticated} = useTokenState();

  const chain = () => {
    if (token && isAuthenticated === true) {
      return Chain('https://project-6124de887ab2767f0af919bb.azurewebsites.net/graphql', {
        headers: {
          Authorization: token,
          'Content-type': 'application/json',
        },
      });
    }
    return Chain('https://project-6124de887ab2767f0af919bb.azurewebsites.net/graphql');
  };

  const validateUserLogin = async (code: string, phoneNumber: string) => {
    try {
      const response = await chain().mutation({
          validate: [
              {
                otpInput: {code, phoneNumber}  
              }, true
          ]
      });
      if (!response) {
        throw new Error('Problem with validateUserLogin');
      }
      return response.validate
    } catch (err) {
      if (err instanceof GraphQLError) {
        let errorType = '';
        err.response.errors?.forEach((v) => {
          if (
            v.message ===
            'lib/Mutation/validate: Invalid OTP code'
          ) {
            errorType = 'Invalid OTP';
            return;
          }
          else {
            errorType = 'Undefined Error'
            return;
          }
        });
        return errorType;
      } else {
        return 'Unknown error';
      }
  };
}

  const login = async (phoneNumber: string, username: string) => {
    try{
      const response = await chain().mutation({
          login: [
            {
               loginInput: {phoneNumber, username}
            }, true
          ]
      });
  
      if (!response) {
        throw new Error('Invalid response from the backend (login)');
      }
      return response.login
    } catch(err) {
      if (err instanceof GraphQLError) {
        let errorType = '';
        err.response.errors?.forEach((v) => {
          if (
            v.message ===
            'lib/Mutation/validate: Invalid OTP code'
          ) {
            errorType = 'Invalid OTP';
            return;
          }
          else {
            errorType = 'Undefined Error'
            return;
          }
        });
        return errorType;
      } else {
        return 'Unknown error';
      }
    }
  };

  const getUserInfo = async () => {
    const response = await chain().query({
        me: {username: true, createdAt: true, wall: {content: {content: true, files: {getUrl: true, type: true}}, createdAt: true}}
    });

    if (!response) {
      throw new Error('Invalid response from the backend (getUserInfo)');
    }
    return response.me
  };

  const getUserByUserName = async (username: string) => {
    const response = await chain().query({
        getUserByUsername: [
            { userGet: { username } },
            {
                username: true,
                createdAt: true,
                wall: {
                    content: {content: true, files: {getUrl: true, type: true}},
                    createdAt: true,
                },
            },
        ],
    });

    if (!response) {
        throw new Error('Invalid response from the backend (getInfoAboutUser)');
    }
    return response.getUserByUsername;
};

  const getWall = async () => {
    try{
      const response = await chain().query({
          me: {wall: {createdAt: true, content: {content: true, files: {getUrl: true, type: true}}}}
      });
      if (!response) {
        throw new Error('Invalid response from the backend (getWall)');
      }
      return response.me.wall
    } catch (err) {
      if (err instanceof GraphQLError) {
        let errorType = '';
        err.response.errors?.forEach((v) => {
          if (
            v.message ===
            'Cannot return null for non-nullable field Content.content.'
          ) {
            errorType = 'Invalid Content';
            return;
          }
          else {
            errorType = 'Undefined Error'
            return;
          }
        });
        return errorType;
      } else {
        return 'Unknown error';
      }
    }
  };

  const uploadFiles = async (files: {name: string, type: string}[]) => {
    const response = await chain().mutation({
      userMutation: {
        uploadFiles: [{files: files}, {getUrl: true, putUrl: true}]
      }
    });
    if (!response) {
      throw new Error('Invalid response from the backend (uploadFiles)')
    }
    return response.userMutation?.uploadFiles
  }

  const postMutationWithFile = async (content: string, files: {getUrl: string, type: string}[]) => {
    const response = await chain().mutation({
        userMutation: {
            post: [{ postCreate: { content, files }}, true],
        },
    });
    if (!response) {
        throw new Error('Invalid response from the backend (postMutationWithFile)');
    }
    return response.userMutation;
};

const postMutationWithoutFile = async (content: string) => {
  const response = await chain().mutation({
      userMutation: {
          post: [{ postCreate: { content } }, true],
      },
  });

  if (!response) {
      throw new Error('Something is wrong');
  }
  return response.userMutation;
};

  return {
    validateUserLogin,
    login,
    postMutationWithFile,
    postMutationWithoutFile,
    getUserInfo, 
    getWall,
    getUserByUserName,
    uploadFiles
  };
};
