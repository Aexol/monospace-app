const lightTheme = () => {
    return {
        background: '#FFFFFF',
        buttonBackground: '#414141',
        publishButtonBackground: '#414141',
        buttonText: '#FFFFFF',
        inputBackground: '#F5F5F5',
        inputText: '#212121',
        largeInputBackground: '#FFFFFF',
        newNoteDominance: '#F2F2F2',
        newNoteText: '#414141',
        logoutBorder: '#F2F2F2',
        logoutText: '#555555',
        text: '#212121',
        error: '#D32F2F',
        inputColor: '#414141',
        logoColor: '#C4C4C4',
        modalBackground: '#FFFFFF',
        dateText: '#8B8787',
        startScreenText: '#555555',
        transparencyColor: '#FFFFFF',
        videoBackground: '#F5F5F5',
    }
}

const darkTheme = () => {
    return{
    background: '#414141',
    buttonBackground: '#E5E5E5',
    buttonText: '#555555',
    publishButtonBackground: '#F2F2F2',
    inputBackground: '#8B8787',
    inputText: '#FFFFFF',
    largeInputBackground: '#8B8787',
    newNoteDominance: '#8B8787',
    newNoteText: '#FFFFFF',
    logoutBorder: '#8B8787',
    logoutText: '#FFFFFF',
    text: '#D8D8D8',
    error: '#EF9A9A',
    inputColor: '#FFFFFF',
    logoColor: '#8B8787',
    modalBackground: '#555555',
    dateText: '#F2F2F2',       
    startScreenText: '#FFFFFF',
    transparencyColor: '#414141',
    videoBackground: '#555555',
    }
}

type ToThemeDict<T> = {
    [P in keyof T]: T[P] extends string ? string : T[P] extends Record<string, any> ? ToThemeDict<T[P]> : never;
};
export type EditorTheme = ToThemeDict<ReturnType<typeof lightTheme>>;
export const DarkTheme = darkTheme() as EditorTheme;
export const LightTheme = lightTheme() as EditorTheme;