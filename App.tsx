/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect } from 'react';
import AppNavigator from '@/src/navigation/AppNavigator';
import { ColorThemeProvider } from '@/src/containers/colorTheme';
import { ApplicationWrapper } from '@/src/components/atoms';
import Orientation from 'react-native-orientation-locker';

console.disableYellowBox = true;

const App = () => {

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  return (
    <ColorThemeProvider>
      <ApplicationWrapper>
        <AppNavigator />
      </ApplicationWrapper>
    </ColorThemeProvider>
  );
};

export default App;
